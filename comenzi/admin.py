from django.contrib import admin

# Register your models here.
from comenzi.models import Comanda, DetaliiComanda, Noutati, Servicii

# admin.site.register(Client)
admin.site.register(Comanda)
admin.site.register(DetaliiComanda)
admin.site.register(Noutati)
admin.site.register(Servicii)
