from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.utils import timezone
from produse.models import Produs


class Comanda(models.Model):
    data = models.DateField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'


class DetaliiComanda(models.Model):
    comanda = models.ForeignKey(Comanda, on_delete=models.CASCADE)
    produs = models.ForeignKey(Produs, on_delete=models.CASCADE)
    cantitate = models.FloatField(blank=True, null=True)

    def __str__(self):
        return f'{self.comanda}'


# current_site = get_current_site(request)
# subiect = "Factura dumneavoastra"
# message = render_to_string('factura_emisa.html', {
#     'user': user,
#     'produse': produse,
#     'domain': current_site.domain,
# })

class Noutati(models.Model):
    nume = models.CharField(max_length=200)
    descriere = models.TextField()

    def __str__(self):
        return f'{self.nume}'


class Servicii(models.Model):
    nume = models.CharField(max_length=200)
    descriere = models.TextField()

    def __str__(self):
        return f'{self.nume}'
