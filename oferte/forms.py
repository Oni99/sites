from django import forms
from django.forms import TextInput, NullBooleanSelect, EmailInput, Textarea, CheckboxInput

from oferte.models import Oferta


class OfertaForm2(forms.ModelForm):
    nume = TextInput()
    class Meta:
        model = Oferta
        fields = ['nume', 'localitate', 'email', 'telefon', 'descriere', 'acord_varsta', 'acord_date',
                  'judete']
        widgets = {
            'nume': TextInput(attrs={'placeholder': 'Nume'}),
            'localitate': TextInput(attrs={'placeholder': 'Localitate'}),
            'email': EmailInput(attrs={'placeholder': 'Email'}),
            'telefon': TextInput(attrs={'placeholder': 'Telefon'}),
            'descriere': Textarea(attrs={'placeholder': 'Spuneți-ne pe scurt ce produse vă interesează și un reprezentat QFORT vă va contacta pentru mai multe informații!'}),
            'acord_varsta': CheckboxInput(attrs={'placeholder': 'acord_varsta'}),
            'acord_date': CheckboxInput(attrs={'placeholder': 'Acord de prelucrare a datelor'}),
            'judete': TextInput(attrs={'placeholder': 'judete'}),
        }

class OfertaForm(forms.Form):
    nume = forms.CharField(max_length=100)
    localitate = forms.CharField(max_length=100)
    email = forms.EmailField(max_length=100)
    telefon = forms.CharField(max_length=100)
    descriere = forms.Textarea()
    judete = forms.CharField(max_length=100)

    def __str__(self):
        return self.nume