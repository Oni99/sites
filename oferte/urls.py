from django.urls import path

from oferte.views import oferta

urlpatterns = [
    path("", oferta, name='oferta'),
]
