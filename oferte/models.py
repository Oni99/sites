from django.db import models


# Create your models here.


class Oferta(models.Model):
    nume = models.CharField(max_length=120)
    localitate = models.CharField(max_length=100)
    email = models.EmailField()
    telefon = models.CharField(max_length=13)
    descriere = models.TextField()
    acord_varsta = models.BooleanField(default=False, blank=True, null=True)
    acord_date = models.BooleanField(default=False, blank=True, null=True)
    judete = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return f"{self.nume}"




