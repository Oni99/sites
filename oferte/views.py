from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import ListView, CreateView

from oferte.forms import OfertaForm
from oferte.models import Oferta


def create_oferta(request_dict):
    nume = request_dict.get('nume')
    email = request_dict.get('email')
    localitate = request_dict.get('localitate')
    judet = request_dict.get('judete')
    descriere = request_dict.get('descriere')
    oferta = Oferta.objects.create(email=email, nume=nume, localitate=localitate, judete=judet, descriere=descriere)
    oferta.save()


def oferta(request):
    print(request.POST)
    form = OfertaForm(request.POST)
    if request.method == 'POST' and 'trimite_oferta' in request.POST:
        print('creating_oferta')
        create_oferta(request.POST)
        # return render(request,'home/home.html',{'form': form})
        return HttpResponse("Ti-am inregistrat oferta cu succes")
        # return render(request, 'home/home.html', {'form': form})
    return render(request, 'home/home.html', {'form': form})


def home(request):
    form = OfertaForm(request.POST)
    if request.method == 'POST' and 'trimite_oferta' in request.POST:
        create_oferta(request.POST)
        return HttpResponse("Ti-am inregistrat oferta cu succes")  # redirect

    return render(request, 'home/home.html', {'form': form})


# class AbcView(CreateView):
#
#
#     def post(self, request, *args, **kwargs):
#         form = OfertaForm(request.POST)
#         if request.method == 'POST' and 'trimite_oferta' in request.POST:
#             create_oferta(request.POST)
#             return HttpResponse("Ti-am inregistrat oferta cu succes")  # redirect
#
#         return render(request, 'abc.html', {'form': form})

