from django.contrib import admin

# Register your models here.
from produse.models import Produs, Culoare

admin.site.register(Produs)
admin.site.register(Culoare)