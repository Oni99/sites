from django.urls import path

from produse.views import ImportanteListView, ProdusCreateView, ProdusListView, ProdusUpdateView, sterge_produs

urlpatterns = [
    path('', ImportanteListView.as_view(), name='produse_lista'),
    path('produs-create/', ProdusCreateView.as_view(), name='creare_produs'),
    path('produs-lista/', ProdusListView.as_view(), name='lista_produse'),
    path('produs-update/<int:pk>', ProdusUpdateView.as_view(), name='actualizare_produs'),
    path('<id>/delete', sterge_produs),
]
