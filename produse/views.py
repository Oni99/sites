from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from oferte.forms import OfertaForm
from oferte.views import create_oferta
from produse.forms import ProdusForm, ProdusUpdateForm
from produse.models import Produs


class ImportanteListView(ListView):
    model = Produs
    template_name = 'produs/produs_list.html'
    context_object_name = 'toate_produsele'

    def post(self, request, *args, **kwargs):
        form = OfertaForm(request.POST)
        if request.method == 'POST' and 'trimite_oferta' in request.POST:
            create_oferta(request.POST)
            return HttpResponse("Ti-am inregistrat oferta cu succes")  # redirect

        return render(request, 'produs/produs_list.html', {'form': form})


class ProdusCreateView(CreateView):
    model = Produs
    template_name = 'produs/produs_create.html'
    success_url = reverse_lazy('oferta')
    form_class = ProdusForm


class ProdusListView(ListView):
    model = Produs
    template_name = 'produs/produse.html'
    context_object_name = 'toate_produsele'


def sterge_produs(request, id):
    obj = Produs.objects.get(pk=id)
    obj.save()
    obj.delete()


class ProdusUpdateView(UpdateView):
    template_name = 'produs/produs_update.html'
    form_class = ProdusUpdateForm
    model = Produs
    success_url = reverse_lazy('lista_produse')

    def post(self, request, *args, **kwargs):
        # form = ProdusForm(request.POST)
        id = self.get_object().pk
        if request.method == 'POST' and 'sterge_produs' in request.POST:
            sterge_produs(request.POST, id)
            return redirect('lista_produse')
        elif request.method == 'POST' and 'salveaza_actualizarile' in request.POST:
            response = super().post(request)
            return response
            # return redirect('lista_produse')
        return render(request, 'produs/produs_update.html', {'form': self.form_class})
