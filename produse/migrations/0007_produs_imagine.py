# Generated by Django 3.2.3 on 2021-05-25 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produse', '0006_auto_20210525_1702'),
    ]

    operations = [
        migrations.AddField(
            model_name='produs',
            name='imagine',
            field=models.ImageField(default=1, upload_to='produses'),
            preserve_default=False,
        ),
    ]
