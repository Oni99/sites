from django import forms
from django.forms import TextInput

from produse.models import Produs


class ProdusForm(forms.ModelForm):
    class Meta:
        model = Produs
        fields = '__all__'
        widgets = {
            'denumire': TextInput(attrs={'placeholder': 'Introduce un nume'}),
            'cod_p': TextInput(attrs={'placeholder': 'Introduce un cod produs'})
        }

    def __str__(self):
        return f'{self.nume}'

    def clean(self):
        codp = self.cleaned_data.get('cod_p')
        toate_produsele = Produs.objects.all()
        for produs in toate_produsele:
            if codp == produs.cod_p:
                msg = " Produsul este deja salvat in baza de date!"
                self._errors['cod_p'] = self.error_class([msg, ])


class ProdusUpdateForm(forms.ModelForm):
    class Meta:
        model = Produs
        fields = '__all__'
