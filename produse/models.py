from django.db import models


# Create your models here.
from django.db.models import IntegerField


class Culoare(models.Model):
    denumire = models.CharField(max_length=255, blank=False, null=False)
    descriere = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.denumire}'


class Produs(models.Model):
    denumire = models.CharField(max_length=255, blank=False, null=False)
    pret = models.FloatField()
    culoare = models.ForeignKey(Culoare, on_delete=models.PROTECT, blank=True, null=True)
    nr_camere = models.IntegerField()
    descriere = models.TextField(null=True)
    importante = models.BooleanField(default=False)
    cod_p = models.CharField(unique=True, max_length=10)
    stoc = models.FloatField(blank=True, null=True)
    um = models.CharField(max_length=2, blank=True, null=True)
    imagine = models.ImageField(upload_to='produses', blank=True, null=True)
    cumparat = IntegerField(default=0)

    def __str__(self):
        return f'{self.denumire}'
