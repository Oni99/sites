# Generated by Django 3.1.7 on 2021-06-07 13:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comenzi', '0008_auto_20210602_1320'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('users', '0004_factura'),
    ]

    operations = [
        migrations.AddField(
            model_name='factura',
            name='produse',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='comenzi.detaliicomanda'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='factura',
            name='user',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to='auth.user'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='factura',
            name='comanda',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='comenzi.comanda'),
        ),
    ]
