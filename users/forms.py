from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import CheckboxSelectMultiple

from comenzi.models import Noutati, Servicii
from produse.models import Produs
from users.models import Client, Companie


class CreareContClient(UserCreationForm):
    class Meta:
        model = Client
        fields = ['username', 'email', 'codcl', 'adresa', 'oras']


class CreareContCompanie(UserCreationForm):
    class Meta:
        model = Companie
        fields = ['username', 'email', 'codcl', 'adresa', 'oras', 'nr_fax']


class RaportPerZile(forms.Form):
    data_inceput = forms.DateInput
    data_sfarsit = forms.DateInput


class ComandaClient(forms.Form):
    user = forms.CharField(max_length=200)


class UltimeleComenzi(forms.Form):
    numar_comenzi = forms.IntegerField()


class Contact(forms.Form):
    email = forms.EmailField()
    judet = forms.CharField(max_length=100)
    localitate = forms.CharField(max_length=100)
    nume = forms.CharField(max_length=100)
    telefon = forms.CharField(max_length=20)
    companie = forms.CharField(max_length=100)
    descriere = forms.Textarea


class Factura(forms.Form):
    nume_user = forms.ModelChoiceField(queryset=User.objects.all())
    produse = forms.ModelMultipleChoiceField(queryset=Produs.objects.all(), widget=CheckboxSelectMultiple)


class NoutatiForm(forms.ModelForm):
    class Meta:
        model = Noutati
        fields = '__all__'


class ServiciiForm(forms.ModelForm):
    class Meta:
        model = Servicii
        fields = '__all__'
