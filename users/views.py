import pprint

from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from comenzi.models import Comanda, DetaliiComanda, Servicii, Noutati
from oferte.forms import OfertaForm
from oferte.views import create_oferta
from produse.models import Produs
from users.forms import CreareContClient, CreareContCompanie, RaportPerZile, ComandaClient, UltimeleComenzi, Contact, \
    Factura, NoutatiForm, ServiciiForm
from users.models import Client
from users.models import Contact as cnt


def register_client(request):
    form = CreareContClient()
    if request.method == 'POST' and 'creare_client' in request.POST:
        if request.POST.get('form') == 'formOne':
            formClient = CreareContClient(request.POST)
            if formClient.is_valid():
                user = formClient.save()
                user.save()
                login(request, user)
                return redirect('/')
    context = {'form': form}
    return render(request, 'registration/inregistrare.html', context)


def register_companie(request):
    form = CreareContCompanie()
    if request.method == 'POST':
        formCompanie = CreareContCompanie(request.POST)
        if formCompanie.is_valid():
            user = formCompanie.save()
            user.save()
            login(request, user)
            return redirect('/')
    context = {'form': form}
    return render(request, 'registration/inregistrare_companii.html', context)


def admin_rapoarte(request):
    return render(request, 'admin_page.html')


def raport_clienti_comenzi(request):
    comenzi = Comanda.objects.all()
    clienti = Client.objects.all()
    raport_clienti_comenzi = len(comenzi) // len(clienti)
    context = {"raport_clienti_comenzi": raport_clienti_comenzi}
    return render(request, 'rapoarte/raport_clienti_comenzi.html', context)


def raport_comenzi(request):
    comenzi = Comanda.objects.all()
    return render(request, 'rapoarte/raport_comenzi.html', {'comenzi': comenzi})


def raport_produse_populare(request):
    produse = Produs.objects.order_by('cumparat')

    return render(request, 'rapoarte/raport_produse_populare.html', {'produse': produse})


def raport_comenzi_zile(request):
    form = RaportPerZile()
    if request.method == 'POST' and 'data_inceput' in request.POST and 'data_sfarsit' in request.POST:
        form = RaportPerZile(request.POST)
        if form.is_valid():
            data_inceput = request.POST.get('data_inceput')
            data_sfarsit = request.POST.get('data_sfarsit')
            comenzi_zile = Comanda.objects.filter(data__range=[f"{data_inceput}", f"{data_sfarsit}"])
            return render(request, 'rapoarte/raport_zile.html', {'comenzi_zile': comenzi_zile})
    return render(request, 'rapoarte/raport_zile.html', {'form': form})


def raport_comanda_client(request):
    form = ComandaClient()
    if request.method == 'POST' and "user" in request.POST:
        form = ComandaClient(request.POST)
        if form.is_valid():
            user = request.POST.get('user')
            comenzi = Comanda.objects.filter(codcl__username=user)
            return render(request, 'rapoarte/raport_comanda_client.html', {'comenzi': comenzi})
    return render(request, 'rapoarte/raport_comanda_client.html', {'form': form})


def raport_ultimele_comenzi(request):
    form = UltimeleComenzi()
    if request.method == 'POST' and 'numar_comenzi' in request.POST:
        form = UltimeleComenzi(request.POST)
        if form.is_valid():
            nr_comenzi = int(form.cleaned_data["numar_comenzi"])
            comenzi = Comanda.objects.all().order_by('-id')[:nr_comenzi]

            return render(request, 'rapoarte/raport_ultimele_comenzi.html', {'comenzi': comenzi})
        else:
            return HttpResponse("ai gresit ceva")
    return render(request, 'rapoarte/raport_ultimele_comenzi.html', {'form': form})


def client_rapoarte(request):
    comenzi = Comanda.objects.filter(user=request.user)
    return render(request, 'profil.html', {'comenzi': comenzi})


def contact(request):
    form = Contact()
    if request.method == 'POST' and 'contact' in request.POST:
        form = Contact(request.POST)
        contact = cnt()
        if form.is_valid():
            email = form.cleaned_data['email']
            judet = form.cleaned_data['judet']
            localitate = form.cleaned_data['localitate']
            nume = form.cleaned_data['nume']
            telefon = form.cleaned_data['telefon']
            companie = form.cleaned_data['companie']
            descriere = request.POST.get('descriere')

            contact.mail = email
            contact.judet = judet
            contact.localitate = localitate
            contact.nume = nume
            contact.telefon = telefon
            contact.companie = companie
            contact.descriere = descriere
            contact.save()
            return render(request, 'home/home.html')

    elif request.method == 'POST' and 'trimite_oferta' in request.POST:
        create_oferta(request.POST)
        return HttpResponse("Ti-am inregistrat oferta cu succes")

    return render(request, 'contact.html', {'form': form})


def facturare(request):
    form = Factura()
    if request.method == 'POST' and 'factura' in request.POST:
        factura_noua = DetaliiComanda()
        the_dict = dict(request.POST)
        form = Factura(request.POST)
        if form.is_valid():
            user = form.cleaned_data['nume_user']
            produse = the_dict['produse']
            pprint.pprint(produse)

            client = User.objects.get(username=user)
            comanda = Comanda.objects.get(codcl=client.id)

            for produs in produse:
                produs1 = Produs.objects.filter(id=produs)
                factura_noua.comanda = comanda
                factura_noua.produs = produs1.cod

                factura_noua.save()
        return render(request, "factura_emisa.html",
                      {"factura": factura_noua})
    return render(request, "factura.html", {'form': form})


def profil(request):
    utilizator = User.objects.filter()
    comenzi = Comanda.objects.filter(user=utilizator)

    return render(request, 'profil.html', {'user': utilizator, 'comenzi': comenzi})


'''SERVICII'''


def servicii(request):
    form = OfertaForm(request.POST)
    servicii = Servicii.objects.all()
    if request.method == 'POST' and 'trimite_oferta' in request.POST:
        create_oferta(request.POST)
        return HttpResponse("Ti-am inregistrat oferta cu succes")
    return render(request, 'servicii.html', {'form': form, 'servicii': servicii})


class ServiciiListView(LoginRequiredMixin, ListView):
    model = Servicii
    template_name = 'servicii/servicii_list.html'
    context_object_name = 'toate_serviciile'


class ServiciiCreateView(LoginRequiredMixin, CreateView):
    template_name = 'servicii/servicii_create.html'
    model = Servicii
    success_url = reverse_lazy('servicii_list')
    form_class = ServiciiForm


class ServiciiDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'servicii/servicii_delete.html'
    model = Servicii
    success_url = reverse_lazy('servicii_list')


class ServiciiUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'servicii/servicii_update.html'
    model = Servicii
    success_url = reverse_lazy('servicii_list')
    form_class = ServiciiForm


'''NOUTATI'''


class NoutatiListView(LoginRequiredMixin, ListView):
    model = Noutati
    template_name = 'noutati/noutati_list.html'
    context_object_name = 'toate_noutatile'


class NoutatiCreateView(LoginRequiredMixin, CreateView):
    template_name = 'noutati/noutati_create.html'
    model = Noutati
    success_url = reverse_lazy('noutati_list')
    form_class = NoutatiForm


class NoutatiUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'noutati/noutati_update.html'
    model = Noutati
    success_url = reverse_lazy('noutati_list')
    form_class = NoutatiForm


class NoutatiDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'noutati/noutati_delete.html'
    model = Noutati
    success_url = reverse_lazy('noutati_list')


def noutati(request):
    form = OfertaForm(request.POST)
    noutati = Noutati.objects.all()
    if request.method == 'POST' and 'trimite_oferta' in request.POST:
        create_oferta(request.POST)
        return HttpResponse("Ti-am inregistrat oferta cu succes")
    return render(request, 'noutati.html', {'form': form, 'noutati': noutati})
