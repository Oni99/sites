from django.urls import path, include
from users.views import register_client, register_companie, raport_clienti_comenzi, admin_rapoarte, raport_comenzi, \
    raport_produse_populare, raport_comenzi_zile, raport_comanda_client, raport_ultimele_comenzi, contact, facturare, \
    servicii, noutati, NoutatiListView, NoutatiCreateView, NoutatiUpdateView, NoutatiDeleteView, ServiciiListView, \
    ServiciiCreateView, ServiciiDeleteView, ServiciiUpdateView, profil

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('inregistrare-clienti/', register_client, name='register_client'),
    path('inregistrare-companii/', register_companie, name='register_companie'),
    path('rapoarte-clienti-comenzi/', raport_clienti_comenzi, name='raport_clienti_comenzi'),
    path('rapoarte/', admin_rapoarte, name='admin_rapoarte'),
    path('raport-comenzi/', raport_comenzi, name='raport_comenzi'),
    path('raport-produse-populare/', raport_produse_populare, name='raport_produse_populare'),
    path('raport-comenzi-zile/', raport_comenzi_zile, name='raport_comenzi_zile'),
    path('raport-comanda-client/', raport_comanda_client, name='raport_comanda_client'),
    path('raport-ultimele-comenzi/', raport_ultimele_comenzi, name='raport_ultimele_comenzi'),
    path('contact/', contact, name='contact'),
    path('factura/', facturare, name='factura'),
    path('servicii/', servicii, name='servicii'),
    path('noutati/', noutati, name='noutati'),
    path('profil/', profil, name='profil'),

    path('noutati-list/', NoutatiListView.as_view(), name='noutati_list'),
    path('noutati-create/', NoutatiCreateView.as_view(), name='noutati_create'),
    path('noutati-update/<int:pk>/', NoutatiUpdateView.as_view(), name='noutati_update'),
    path('noutati-delete/<int:pk>/', NoutatiDeleteView.as_view(), name='noutati_delete'),

    path('servicii-list/', ServiciiListView.as_view(), name='servicii_list'),
    path('servicii-create/', ServiciiCreateView.as_view(), name='servicii_create'),
    path('servicii-delete/<int:pk>', ServiciiDeleteView.as_view(), name='servicii_delete'),
    path('servicii-update/<int:pk>', ServiciiUpdateView.as_view(), name='servicii_update'),
]
