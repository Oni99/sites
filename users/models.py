from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from django.utils import timezone

from comenzi.models import Comanda, DetaliiComanda


class Client(User):
    # user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    codcl = models.CharField(max_length=30)
    adresa = models.CharField(max_length=80)
    oras = models.CharField(max_length=30)
    is_client = models.BooleanField(default=1)


class Companie(User):
    # user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    codcl = models.CharField(max_length=30)
    adresa = models.CharField(max_length=80)
    oras = models.CharField(max_length=30)
    nr_fax = models.CharField(max_length=20)
    is_company = models.BooleanField(default=1)


class Contact(models.Model):
    mail = models.EmailField(unique=True)
    judet = models.CharField(max_length=100)
    localitate = models.CharField(max_length=100)
    nume = models.CharField(max_length=100)
    telefon = models.CharField(max_length=20)
    companie = models.CharField(max_length=100)
    descriere = models.TextField()


class Factura(models.Model):
    comanda = models.ForeignKey(Comanda, on_delete=models.CASCADE)
    nr_fact = models.CharField(max_length=200)
    date = models.DateField(default=timezone.now)
